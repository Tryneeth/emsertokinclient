class Product {
  String name;
  double price;
  String image;
  Product({this.name, this.price, this.image});
}
