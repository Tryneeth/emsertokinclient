import 'package:flutter/material.dart';

import 'product.dart';

class Category {
  String name;
  List<Product> products;

  Category({@required this.name, @required this.products});
}
