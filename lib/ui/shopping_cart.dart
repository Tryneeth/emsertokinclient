import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'shared/shared_components.dart';

class ShoppingCartPage extends StatelessWidget {
  const ShoppingCartPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MainSheet(
      child: Container(
        margin: const EdgeInsets.only(top: 30),
        child: Column(
          children: <Widget>[
            Expanded(
              child: Container(
                child: Column(
                  children: <Widget>[
                    Row(children: <Widget>[
                      Wrap(
                        crossAxisAlignment: WrapCrossAlignment.center,
                        children: <Widget>[
                          CategoryIcon(
                            icon: EvaIcons.shoppingCartOutline,
                            iconColor: Theme.of(context).primaryColor,
                            backgroundColor:
                                Theme.of(context).primaryColor.withOpacity(0.1),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Text("En tu carrito",
                                style: TextStyle(
                                  inherit: false,
                                  fontSize: 17,
                                  fontWeight: FontWeight.w900,
                                  color: CupertinoColors.label,
                                )),
                          ),
                        ],
                      ),
                      Spacer(),
                      Text(
                        "4",
                        style: TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontSize: 18,
                            fontWeight: FontWeight.bold),
                      )
                    ]),
                    Expanded(child: ListView(children: <Widget>[
                      ShoppingCartItem(),
                      ShoppingCartItem(),
                      ShoppingCartItem(),
                      ShoppingCartItem(),
                    ],),)
                  ],
                ),
                padding: const EdgeInsets.symmetric(horizontal: 16),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                  color: Colors.blue.withOpacity(0.1),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20)),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                      color: Colors.blueGrey.withOpacity(0.1),
                      spreadRadius: 2,
                      blurRadius: 3,
                      offset: Offset(0, 0),
                    ),
                  ]),
              height: MediaQuery.of(context).size.height * 0.2,
              alignment: Alignment.topCenter,
              child: Container(
                height: 35,
                margin:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                ),
                child: Center(
                  child: Text(
                    "Ir a pagar - \$31.98",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
