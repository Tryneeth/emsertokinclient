import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tokin_client/models/category.dart';
import 'shared/shared_components.dart';

class CategoryPage extends StatelessWidget {
  final List<Category> categories;
  CategoryPage({Key key, this.categories}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> children =
        categories.map<Widget>((e) => CategoryProducts(category: e)).toList();
    children.add(Container(padding: const EdgeInsets.symmetric(vertical: 30)));

    return MainSheet(
      child: Column(
        children: <Widget>[
          Container(
            height: 40,
            decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(color: Colors.grey.withOpacity(0.3)))),
            padding: const EdgeInsets.symmetric(horizontal: 10),
            width: MediaQuery.of(context).size.width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                InkWell(
                  onTap: () {
                    print("cosa");
                  },
                  child: Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 5, vertical: 2),
                      decoration: BoxDecoration(
                          border: Border(
                              left: BorderSide(
                                  color: Colors.grey.withOpacity(0.3)),
                              right: BorderSide(
                                  color: Colors.grey.withOpacity(0.3)))),
                      child: Icon(
                        EvaIcons.layersOutline,
                        size: 25,
                      )),
                ),
                InkWell(
                  onTap: () {},
                  child: Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 5, vertical: 2),
                      child: Icon(
                        EvaIcons.funnel,
                        size: 25,
                      )),
                )
              ],
            ),
          ),
          Container(
            child: Expanded(
              child: ListView(shrinkWrap: true, children: children),
            ),
          )
        ],
      ),
    );
  }
}
