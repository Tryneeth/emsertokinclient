import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'shared/shared_components.dart';

class ChallengesPage extends StatelessWidget {
  const ChallengesPage({Key key}) : super(key: key);

  final String textInfo =
      "Gracias por tomarte unos minutos para realizar los siguientes desafíos";

  @override
  Widget build(BuildContext context) {
    return MainSheet(
      info: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.green.withOpacity(0.08),
        ),
        margin: const EdgeInsets.symmetric(horizontal: 32, vertical: 15),
        padding: const EdgeInsets.all(10),
        child: Text(textInfo,
            style: TextStyle(fontWeight: FontWeight.w700, fontSize: 14),
            textAlign: TextAlign.center),
      ),
      child: Container(
        margin: const EdgeInsets.only(top: 30),
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Expanded(
          child: ListView(shrinkWrap: true, children: <Widget>[
            Container(
              padding: const EdgeInsets.only(bottom: 10),
              child: Row(children: <Widget>[
                Wrap(
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: <Widget>[
                    CategoryIcon(
                      icon: EvaIcons.fileTextOutline,
                      iconColor: Theme.of(context).primaryColor,
                      backgroundColor: Color.fromRGBO(232, 242, 251, 1),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Text("Desafío 1",
                          style: TextStyle(
                            inherit: false,
                            fontFamily: '.SF Pro Display',
                            fontSize: 17,
                            fontWeight: FontWeight.w900,
                            color: CupertinoColors.label,
                          )),
                    ),
                  ],
                ),
                Spacer(),
                CategoryButton()
              ]),
            ),
            Container(
              padding: const EdgeInsets.only(bottom: 10),
              child: Row(children: <Widget>[
                Wrap(
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: <Widget>[
                    CategoryIcon(
                      icon: EvaIcons.fileTextOutline,
                      iconColor: Colors.yellow,
                      backgroundColor: Colors.yellow.withOpacity(0.09),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Text("Desafío 2 - Empezado",
                          style: TextStyle(
                            inherit: false,
                            fontFamily: '.SF Pro Display',
                            fontSize: 17,
                            fontWeight: FontWeight.w900,
                            color: CupertinoColors.label,
                          )),
                    ),
                  ],
                ),
                Spacer(),
                CategoryButton()
              ]),
            ),
            Container(
              padding: const EdgeInsets.only(bottom: 10),
              child: Row(children: <Widget>[
                Wrap(
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: <Widget>[
                    CategoryIcon(
                      icon: EvaIcons.fileTextOutline,
                      iconColor: Colors.green,
                      backgroundColor: Colors.green.withOpacity(0.07),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Text("Desafío 3 - Terminado",
                          style: TextStyle(
                            inherit: false,
                            fontFamily: '.SF Pro Display',
                            fontSize: 17,
                            fontWeight: FontWeight.w900,
                            color: CupertinoColors.label,
                          )),
                    ),
                  ],
                ),
                Spacer(),
                CategoryButton()
              ]),
            ),
          ]),
        ),
      ),
    );
  }
}
