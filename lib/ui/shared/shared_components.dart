import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tokin_client/models/category.dart';

class NavigationButton extends StatelessWidget {
  final IconData icon;
  const NavigationButton({Key key, @required this.icon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      width: 40,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.25),
              spreadRadius: 1,
              blurRadius: 4,
              offset: Offset(0, 1),
            ),
          ]),
      child: Icon(
        icon,
        size: 25,
      ),
    );
  }
}

class CategoryButton extends StatelessWidget {
  const CategoryButton({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      width: 40,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
          border:
              Border.all(color: Theme.of(context).primaryColor, width: 0.3)),
      child: Icon(EvaIcons.arrowIosForwardOutline,
          size: 25, color: Theme.of(context).primaryColor),
    );
  }
}

class CategoryIcon extends StatelessWidget {
  final IconData icon;
  final Color backgroundColor;
  final Color iconColor;
  const CategoryIcon(
      {Key key,
      @required this.icon,
      @required this.backgroundColor,
      @required this.iconColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      width: 40,
      decoration: BoxDecoration(
          color: backgroundColor, borderRadius: BorderRadius.circular(8)),
      child: Icon(icon, size: 25, color: iconColor),
    );
  }
}

class MainSheet extends StatelessWidget {
  final Widget child;
  final Widget info;
  const MainSheet({Key key, @required this.child, this.info}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
        color: Colors.grey.shade50,
        child: Column(
          children: <Widget>[
            info ?? Container(),
            Expanded(
                child: Container(
                    margin: EdgeInsets.only(top: info == null ? 30 : 15),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(35),
                            topRight: Radius.circular(35)),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 2,
                            blurRadius: 3,
                            offset: Offset(0, 0),
                          ),
                        ]),
                    child: child)),
          ],
        ));
  }
}

class ShoppingCartItem extends StatelessWidget {
  final String image;
  final String name;
  final String description;
  const ShoppingCartItem({Key key, this.image, this.name, this.description})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.width * 0.15,
      margin: const EdgeInsets.symmetric(horizontal: 18, vertical: 10),
      child: Row(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                color: Theme.of(context).primaryColor.withOpacity(0.1),
                borderRadius: BorderRadius.circular(10)),
            width: MediaQuery.of(context).size.width * 0.15,
            padding: const EdgeInsets.all(8),
            child: Image.asset(
              "assets/images/helado.jpg",
              scale: 1,
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            alignment: Alignment.topCenter,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("HELADOS",
                    style:
                        TextStyle(fontSize: 10, fontWeight: FontWeight.bold)),
                Text("HELADOS BON O BON CITOS",
                    style: TextStyle(
                        fontSize: 10,
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).primaryColor)),
              ],
            ),
          ),
          Spacer(),
          Container(
            child: Text("\$20.00",
                style: TextStyle(fontSize: 11, fontWeight: FontWeight.bold)),
            alignment: Alignment.topCenter,
          )
        ],
      ),
    );
  }
}

class CategoryProducts extends StatefulWidget {
  final Category category;
  CategoryProducts({Key key, @required this.category}) : super(key: key);

  @override
  _CategoryProductsState createState() => _CategoryProductsState();
}

class _CategoryProductsState extends State<CategoryProducts> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
      child: Column(
        children: <Widget>[
          Row(children: <Widget>[
            CategoryIcon(
              icon: EvaIcons.star,
              iconColor: Theme.of(context).primaryColor,
              backgroundColor: Color.fromRGBO(232, 242, 251, 1),
            ),
            Spacer(
              flex: 1,
            ),
            Text("Destacados",
                style: TextStyle(
                  inherit: false,
                  fontFamily: '.SF Pro Display',
                  fontSize: 20,
                  fontWeight: FontWeight.w900,
                  color: CupertinoColors.label,
                )),
            Spacer(
              flex: 8,
            ),
            CategoryButton()
          ]),
          Container(
            width: MediaQuery.of(context).size.width,
            constraints: BoxConstraints(minHeight: 220, maxHeight: 260),
            height: 255,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                CategoryItem(
                  isInShoppingCart: false,
                ),
                CategoryItem(
                  isInShoppingCart: true,
                ),
                CategoryItem(
                  isInShoppingCart: false,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class CategoryItem extends StatefulWidget {
  bool isInShoppingCart;
  CategoryItem({Key key, @required this.isInShoppingCart}) : super(key: key);

  @override
  _CategoryItemState createState() => _CategoryItemState();
}

class _CategoryItemState extends State<CategoryItem> {
  bool favorite;
  int itemAmount;
  @override
  void initState() {
    super.initState();
    favorite = true;
    itemAmount = 1;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 4),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
      elevation: 3,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
        width: (MediaQuery.of(context).size.width - 48) / 2,
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  InkWell(
                      highlightColor: Colors.transparent,
                      splashColor: Colors.transparent,
                      onTap: () => setState(() {
                            favorite = !favorite;
                          }),
                      child: Icon(
                        favorite ? EvaIcons.heart : EvaIcons.heartOutline,
                        color: Theme.of(context).primaryColor,
                      ))
                ],
              ),
            ),
            Container(
              constraints: BoxConstraints(minHeight: 100, maxHeight: 105),
              child: Image.asset(
                "assets/images/helado.jpg",
                width: (MediaQuery.of(context).size.width - 50) / 3,
              ),
            ),
            Text("HELADO BON O BON CITOS",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 11.0,
                  fontWeight: FontWeight.w600,
                  letterSpacing: -0.41,
                  color: CupertinoColors.label,
                )),
            Text(
              "Helado de crema de maní cubierto con chocolate con leche.",
              textAlign: TextAlign.center,
              style: CupertinoTheme.of(context).textTheme.tabLabelTextStyle,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 5.0),
              child: Text("\$20.99",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Theme.of(context).primaryColor, fontSize: 12)),
            ),
            Divider(),
            _shoppingActions()
          ],
        ),
      ),
    );
  }

  Widget _shoppingActions() {
    if (!widget.isInShoppingCart) {
      return Center(
        child: Wrap(
          crossAxisAlignment: WrapCrossAlignment.center,
          children: <Widget>[
            Icon(EvaIcons.shoppingBagOutline,
                color: Color.fromRGBO(81, 186, 153, 1)),
            Text(
              "Comprar",
              style: TextStyle(color: Color.fromRGBO(81, 186, 153, 1)),
            )
          ],
        ),
      );
    } else {
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 5.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
                padding: const EdgeInsets.all(3),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.white,
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: Colors.grey.shade300,
                          spreadRadius: 0.8,
                          blurRadius: 2,
                          offset: Offset(0, 1)),
                    ]),
                child: InkWell(
                  onTap: () => setState(() {
                    itemAmount--;
                  }),
                  child: Icon(
                    EvaIcons.minus,
                    size: 18,
                  ),
                )),
            Text(
              "${itemAmount ?? 0}",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Container(
                padding: const EdgeInsets.all(3),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.white,
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: Colors.grey.shade300,
                          spreadRadius: 0.8,
                          blurRadius: 2,
                          offset: Offset(0, 1)),
                    ]),
                child: InkWell(
                  onTap: () => setState(() {
                    itemAmount++;
                  }),
                  child: Icon(
                    EvaIcons.plus,
                    size: 18,
                  ),
                ))
          ],
        ),
      );
    }
  }
}
