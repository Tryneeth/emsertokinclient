import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'shared/shared_components.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MainSheet(
      info: Container(
          margin: const EdgeInsets.symmetric(horizontal: 32, vertical: 10),
          padding: const EdgeInsets.all(10),
          child: Column(
            children: <Widget>[
              CircleAvatar(
                child: Image.asset(
                  "assets/images/helado.jpg",
                ),
                backgroundColor: Colors.red,
                foregroundColor: Colors.red,
                radius: 30,
              ),
              Text(
                "Andrés Escobar",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              Text(
                "Id Cliente: 1307",
                style: TextStyle(fontSize: 10, fontWeight: FontWeight.bold),
              )
            ],
          )),
      child: Container(
        margin: const EdgeInsets.only(top: 30),
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Expanded(
          child: ListView(shrinkWrap: true, children: <Widget>[
            Container(
              padding: const EdgeInsets.only(bottom: 10),
              child: Row(children: <Widget>[
                Wrap(
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: <Widget>[
                    CategoryIcon(
                      icon: EvaIcons.bellOutline,
                      iconColor: Theme.of(context).primaryColor,
                      backgroundColor:
                          Theme.of(context).primaryColor.withOpacity(0.1),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Text("Notificaciones",
                          style: TextStyle(
                            inherit: false,
                            fontFamily: '.SF Pro Display',
                            fontSize: 17,
                            fontWeight: FontWeight.w900,
                            color: CupertinoColors.label,
                          )),
                    ),
                  ],
                ),
                Spacer(),
                CategoryButton()
              ]),
            ),
            Container(
              padding: const EdgeInsets.only(bottom: 10),
              child: Row(children: <Widget>[
                Wrap(
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: <Widget>[
                    CategoryIcon(
                      icon: EvaIcons.fileTextOutline,
                      iconColor: Theme.of(context).primaryColor,
                      backgroundColor:
                          Theme.of(context).primaryColor.withOpacity(0.1),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Text("Mis Órdenes",
                          style: TextStyle(
                            inherit: false,
                            fontFamily: '.SF Pro Display',
                            fontSize: 17,
                            fontWeight: FontWeight.w900,
                            color: CupertinoColors.label,
                          )),
                    ),
                  ],
                ),
                Spacer(),
                CategoryButton()
              ]),
            ),
            Container(
              padding: const EdgeInsets.only(bottom: 10),
              child: Row(children: <Widget>[
                Wrap(
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: <Widget>[
                    CategoryIcon(
                      icon: EvaIcons.paperPlaneOutline,
                      iconColor: Theme.of(context).primaryColor,
                      backgroundColor:
                          Theme.of(context).primaryColor.withOpacity(0.1),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Text("Mis Direcciones",
                          style: TextStyle(
                            inherit: false,
                            fontFamily: '.SF Pro Display',
                            fontSize: 17,
                            fontWeight: FontWeight.w900,
                            color: CupertinoColors.label,
                          )),
                    ),
                  ],
                ),
                Spacer(),
                CategoryButton()
              ]),
            ),
            Container(
              padding: const EdgeInsets.only(bottom: 10),
              child: Row(children: <Widget>[
                Wrap(
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: <Widget>[
                    CategoryIcon(
                      icon: EvaIcons.creditCardOutline,
                      iconColor: Theme.of(context).primaryColor,
                      backgroundColor:
                          Theme.of(context).primaryColor.withOpacity(0.1),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Text("Información de pago",
                          style: TextStyle(
                            inherit: false,
                            fontFamily: '.SF Pro Display',
                            fontSize: 17,
                            fontWeight: FontWeight.w900,
                            color: CupertinoColors.label,
                          )),
                    ),
                  ],
                ),
                Spacer(),
                CategoryButton()
              ]),
            ),
            Container(
              padding: const EdgeInsets.only(bottom: 10),
              child: Row(children: <Widget>[
                Wrap(
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: <Widget>[
                    CategoryIcon(
                      icon: EvaIcons.starOutline,
                      iconColor: Theme.of(context).primaryColor,
                      backgroundColor:
                          Theme.of(context).primaryColor.withOpacity(0.1),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Text("Premios",
                          style: TextStyle(
                            inherit: false,
                            fontFamily: '.SF Pro Display',
                            fontSize: 17,
                            fontWeight: FontWeight.w900,
                            color: CupertinoColors.label,
                          )),
                    ),
                  ],
                ),
                Spacer(),
                CategoryButton()
              ]),
            ),
            Container(
              padding: const EdgeInsets.only(bottom: 10),
              child: Row(children: <Widget>[
                Wrap(
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: <Widget>[
                    CategoryIcon(
                      icon: EvaIcons.settings2Outline,
                      iconColor: Theme.of(context).primaryColor,
                      backgroundColor:
                          Theme.of(context).primaryColor.withOpacity(0.1),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Text("Ajustes",
                          style: TextStyle(
                            inherit: false,
                            fontFamily: '.SF Pro Display',
                            fontSize: 17,
                            fontWeight: FontWeight.w900,
                            color: CupertinoColors.label,
                          )),
                    ),
                  ],
                ),
                Spacer(),
                CategoryButton()
              ]),
            )
          ]),
        ),
      ),
    );
  }
}
