import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:tokin_client/models/category.dart';
import 'shared/shared_components.dart';

class HomePage extends StatefulWidget {
  final List<Category> categories;
  const HomePage({Key key, this.categories}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String currentGroup = "all";
  static TextStyle controlsStyle = TextStyle(fontSize: 10);
  Map<String, Widget> controlLabels = {
    "all": Text(
      "Todos",
      style: controlsStyle,
    ),
    "oferts": Text(
      "Ofertas",
      style: controlsStyle,
    ),
    "general": Text(
      "General",
      style: controlsStyle,
    ),
    "throws": Text(
      "Lanzamientos",
      style: controlsStyle,
    )
  };

  @override
  Widget build(BuildContext context) {
    List<Widget> categoriesWidgets = widget.categories
        .map<Widget>((e) => CategoryProducts(category: e))
        .toList();
    categoriesWidgets
        .add(Container(padding: const EdgeInsets.symmetric(vertical: 30)));

    return MainSheet(
      info: Container(
          padding: const EdgeInsets.only(left: 30, right: 30, top: 10),
          child: CupertinoSlidingSegmentedControl(
              backgroundColor: Colors.transparent,
              groupValue: currentGroup,
              children: controlLabels,
              onValueChanged: (value) {
                setState(() {
                  currentGroup = value;
                });
              })),
      child: Column(
        children: <Widget>[
          Container(
            height: 40,
            decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(color: Colors.grey.withOpacity(0.3)))),
            padding: const EdgeInsets.symmetric(horizontal: 10),
            width: MediaQuery.of(context).size.width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                InkWell(
                  onTap: () {
                    print("cosa");
                  },
                  child: Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 5, vertical: 2),
                      decoration: BoxDecoration(
                          border: Border(
                              left: BorderSide(
                                  color: Colors.grey.withOpacity(0.3)),
                              right: BorderSide(
                                  color: Colors.grey.withOpacity(0.3)))),
                      child: Icon(
                        EvaIcons.layersOutline,
                        size: 25,
                      )),
                ),
                InkWell(
                  onTap: () {},
                  child: Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 5, vertical: 2),
                      child: Icon(
                        EvaIcons.funnel,
                        size: 25,
                      )),
                )
              ],
            ),
          ),
          Container(
            child: Expanded(
              child: ListView(shrinkWrap: true, children: categoriesWidgets),
            ),
          )
        ],
      ),
    );
  }
}
