import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tokin_client/models/category.dart';
import 'package:tokin_client/models/product.dart';
import 'package:tokin_client/ui/categories.dart';
import 'package:tokin_client/ui/challenges.dart';

import 'home.dart';
import 'profile.dart';
import 'shared/shared_components.dart';
import 'shopping_cart.dart';

class MainPageUI extends StatefulWidget {
  MainPageUI({Key key}) : super(key: key);

  @override
  _MainPageUIState createState() => _MainPageUIState();
}

class _MainPageUIState extends State<MainPageUI> {
  int selectedIndex;
  Widget selectPage;
  List<Widget> pages = <Widget>[
    HomePage(
        categories: List<Category>.filled(
            6,
            Category(name: "Destacados", products: <Product>[
              Product(
                  name: "HELADO BON O BON CITOS",
                  image: "helados.jpg",
                  price: 20.99),
              Product(
                  name: "HELADO BON O BON CITOS",
                  image: "helados.jpg",
                  price: 20.99),
              Product(
                  name: "HELADO BON O BON CITOS",
                  image: "helados.jpg",
                  price: 20.99)
            ]))),
    CategoryPage(
        categories: List<Category>.filled(
            6,
            Category(name: "Destacados", products: <Product>[
              Product(
                  name: "HELADO BON O BON CITOS",
                  image: "helados.jpg",
                  price: 20.99),
              Product(
                  name: "HELADO BON O BON CITOS",
                  image: "helados.jpg",
                  price: 20.99),
              Product(
                  name: "HELADO BON O BON CITOS",
                  image: "helados.jpg",
                  price: 20.99)
            ]))),
    ShoppingCartPage(),
    ChallengesPage(),
    ProfilePage()
  ];

  @override
  void initState() {
    selectedIndex = 0;
    selectPage = pages[selectedIndex];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (selectPage == null) {
      return Text("asdasd");
    }
    return CupertinoPageScaffold(
      navigationBar: _navigationBar(),
      child: SafeArea(
          child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          selectPage,
          Positioned(
              bottom: 15,
              child: BottomBar(
                onChangePage: (int pageIndex) {
                  if (pageIndex <= pages.length)
                    setState(() {
                      selectedIndex = pageIndex;
                      selectPage = pages[pageIndex];
                    });
                },
              ))
        ],
      )),
    );
  }

  Widget _navigationBar() {
    String title;
    bool enableLeading;
    bool enableTrailing;
    IconData trailingIcon;
    switch (selectedIndex) {
      case 0:
        title = "Hola, Andrés";
        enableLeading = true;
        enableTrailing = true;
        trailingIcon = EvaIcons.searchOutline;
        break;
      case 1:
        title = "Catálogo";
        enableLeading = false;
        enableTrailing = true;
        trailingIcon = EvaIcons.searchOutline;
        break;
      case 2:
        title = "Carrito de compra";
        enableLeading = false;
        enableTrailing = true;
        trailingIcon = EvaIcons.moreHorizotnalOutline;
        break;
      case 3:
        title = "Desafíos";
        enableLeading = false;
        enableTrailing = true;
        trailingIcon = EvaIcons.optionsOutline;
        break;
      case 4:
        title = "Perfil";
        enableLeading = false;
        enableTrailing = false;
        trailingIcon = EvaIcons.searchOutline;
        break;
      default:
    }
    return CupertinoNavigationBar(
        padding: const EdgeInsetsDirectional.fromSTEB(16, 10, 16, 0),
        leading: enableLeading
            ? NavigationButton(
                icon: EvaIcons.arrowIosBackOutline,
              )
            : null,
        automaticallyImplyLeading: false,
        middle: Text(
          title,
          style: CupertinoTheme.of(context).textTheme.navTitleTextStyle,
        ),
        trailing: enableTrailing
            ? NavigationButton(
                icon: trailingIcon,
              )
            : null,
        border: null,
        backgroundColor: Colors.grey.shade50);
  }
}

typedef OnChangePage = void Function(int);

class BottomBar extends StatefulWidget {
  OnChangePage onChangePage;
  BottomBar({Key key, @required this.onChangePage}) : super(key: key);

  @override
  _BottomBarState createState() => _BottomBarState();
}

class _BottomBarState extends State<BottomBar> {
  int selectedIndex;

  @override
  void initState() {
    selectedIndex = 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width - 32,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 2,
                    blurRadius: 3,
                    offset: Offset(0, 3),
                  ),
                ],
              ),
              child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 14),
                  child: _cupertinoTabBar())),
        ],
      ),
    );
  }

  Widget _cupertinoTabBar() {
    return CupertinoTabBar(
      border: null,
      activeColor: Color.fromRGBO(12, 156, 221, 1),
      inactiveColor: Color.fromRGBO(131, 181, 218, 1),
      backgroundColor: Colors.white,
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
            icon: Icon(
          EvaIcons.homeOutline,
          size: 24,
        )),
        BottomNavigationBarItem(
            icon: Icon(
          EvaIcons.listOutline,
          size: 24,
        )),
        BottomNavigationBarItem(
            icon: Stack(
          children: <Widget>[
            Icon(
              EvaIcons.shoppingCartOutline,
              size: 24,
            ),
            Positioned(
                right: 0,
                top: 0,
                child: Container(
                  padding: const EdgeInsets.all(3),
                  decoration:
                      BoxDecoration(shape: BoxShape.circle, color: Colors.red),
                  child: Text(
                    "10",
                    style: TextStyle(color: Colors.white, fontSize: 7),
                  ),
                )),
          ],
        )),
        BottomNavigationBarItem(
            icon: Icon(
          EvaIcons.awardOutline,
          size: 24,
        )),
        BottomNavigationBarItem(
            icon: Icon(
          EvaIcons.personOutline,
          size: 24,
        )),
      ],
      currentIndex: selectedIndex,
      onTap: (index) {
        setState(() {
          selectedIndex = index;
          widget.onChangePage(index);
        });
      },
    );
  }
}
